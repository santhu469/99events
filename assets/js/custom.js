
$(window).bind('scroll', function() {
    // var navHeight = $( window ).height() - 70;
    if ($(window).scrollTop()) {
        $('.nav-fix').addClass('fixed');
    } else {
        $('.nav-fix').removeClass('fixed');
    }
});

$(document).ready(function() {
    $("#mySidenav li a").click(function() {
        $("#mySidenav").css("transform", "translateX(-100%)");
        $('body').css({ overflow: "auto", opacity: "9" });
    });
});

function openNav() {
    // document.getElementById("mySidenav").style.transform = "translateX(0%)";
    $("#mySidenav").css("transform", "translateX(0%)");
    $('body').css({ overflow: "hidden", opacity: "0.8" });
}

function closeNav() {
    $("#mySidenav").css("transform", "translateX(-100%)");
    $('body').css({ overflow: "auto", opacity: "9" });
}